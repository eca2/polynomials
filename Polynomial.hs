
import Clash.Prelude

type Clk = Clock System
type Rst = Reset System
type Sig = Signal System

type Value = Signed 16

-- Student information:
--  Student 1
--    lastname: Battistig
--    student number: s1716239
--  Student 2
--    lastname: Pennestri
--    student number: s2382660


-- NOTE: topentity functions are at the bottom
-- comment and uncomment the functions for each assignment




polyCoef:: Vec 5 Value
polyCoef = (2:>3:>4:>5:>2:>Nil) -- (a0,a1,a2, ....)

-----------------------------------------------------------
-- Assignment 1
-- f0p f0m1 f0m2
-----------------------------------------------------------
pow :: KnownNat n => Value -> SNat n -> Value
pow x sn = resize (x ^ sn)

f0p:: Value -> Value
f0p x =  (polyCoef !! 4) * (pow x d4) + (polyCoef !! 3) * (pow x d3) + (polyCoef !! 2) * (pow x d2) + (polyCoef !! 1) * (pow x d1) + (polyCoef !! 0)

f0m1:: Value -> Value
f0m1 x = (polyCoef !! 4) * x * x * x * x + (polyCoef !! 3) * x * x * x  + (polyCoef !! 2) * x * x  + (polyCoef !! 1) * x  + (polyCoef !! 0)

f0m2:: Value -> Value
f0m2 x =  (polyCoef !! 4) * (x * x * x * x) + (polyCoef !! 3) * (x * x * x)  + (polyCoef !! 2) * (x * x)  + (polyCoef !! 1) * x  + (polyCoef !! 0)
-----------------------------------------------------------
-- Assignment 2
-- f0f
-----------------------------------------------------------
f0 :: Value -> Value
f0 x = x * ( (polyCoef !! 4) * (pow x d3) + (polyCoef !! 3) * (pow x d2) + (polyCoef !! 2) * (pow x d1) + (polyCoef !! 1) )
-----------------------------------------------------------
-- Assignment 3
-- f0hof
----------------------------------------------------------

f0hof:: Value -> Value
f0hof x = result
        where
           x_vec = repeat x :: Vec 4 Value
           x_pow_vec = scanl (*) 1 x_vec  -- (1,x,x^2, ....)
           result = sum(zipWith (*) polyCoef x_pow_vec)

-----------------------------------------------------------
-- Assignment 4
-- f0mealy
----------------------------------------------------------
f0mealy:: Vec 8 Value -> Value -> (Vec 8 Value, Maybe(Value))
f0mealy state x = (state' , out)
              where 
                
                -- Select Different componets from the state ----
                accReg = state !! 0
                xPowReg = state !! 1
                stateReg = state !! 2
                xVecReg = select d3 d1 d5 state

                ---------- State definition ----------------
                done = stateReg == 7
                start = stateReg == 0
                state1 = stateReg == 1
                state2 = stateReg == 2
                state3 = stateReg == 3
                state4 = stateReg == 4
                state5 = stateReg == 5
                state6 = stateReg == 6
                
                nextState --- next state definition
                    |done = 0
                    |start = 1
                    |state1 = 2
                    |state2 = 3
                    |state3 = 4
                    |state4 = 5
                    |state5 = 6
                    |state6 = 7
                    |otherwise = 0
                ----------------------------------------------
                
                theALU
                    |start || state1 || state2 || state3 = xPowReg * x
                    |state4 = accReg + (polyCoef !! 1) * (xVecReg !! 1)
                    |state5 = accReg + (polyCoef !! 2) * (xVecReg !! 2)
                    |state6 = accReg + (polyCoef !! 3) * (xVecReg !! 3)
                    |done = accReg + (polyCoef !! 4) * (xVecReg !! 4)
                    |otherwise = 0

                nextXPowReg
                    |done = 1
                    |start || state1 || state2 || state3 = theALU
                    |otherwise = xPowReg

                nextXVecReg
                    |done = (0:>0:>0:>0:>1:>Nil)
                    |start || state1 || state2 || state3 = xVecReg <<+ theALU
                    |otherwise = xVecReg

                nextAccReg
                    |start = head polyCoef
                    |state4 ||state5 ||state6 ||done = theALU  
                    |otherwise = accReg

                state' = (nextAccReg:>nextXPowReg:>nextState:>Nil) ++ nextXVecReg

                out
                   |done = Just(theALU)
                   |otherwise = Nothing


f0mealySim:: HiddenClockResetEnable dom  => Signal dom Value -> Signal dom (Maybe(Value))
f0mealySim = mealy f0mealy ((head polyCoef):>1:>0:>0:>0:>0:>0:>1:>Nil)
-- Use as : simulate @System f0mealySim (toList (replicate d7 %value of x% ))

-----------------------------------------------------------
-- topEntity's
-----------------------------------------------------------

-- Assignment 1
topEntity:: Value -> (Value,Value,Value)
topEntity x = (f0p x, f0m1 x, f0m2 x)
{-# NOINLINE f0p #-}
{-# NOINLINE f0m1 #-}
{-# NOINLINE f0m2 #-}
-- Assignment 2
--topEntity  = f0

-- Assignment 3
--topEntity = f0hof

-- Assignment 4
--topEntity :: Clk -> Rst  -> Sig Value -> Sig (Maybe(Value))
--topEntity clk rst x = withClockResetEnable clk rst enableGen (mealy f0mealy ((head polyCoef):>1:>0:>0:>0:>0:>0:>1:>Nil) ) x
--{-# NOINLINE f0mealy #-}