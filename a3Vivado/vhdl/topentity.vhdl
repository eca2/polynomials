-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.main_types.all;

entity topentity is
  port(\c$arg\ : in signed(15 downto 0);
       result  : out signed(15 downto 0));
end;

architecture structural of topentity is
  signal \c$x_app_arg\ : main_types.array_of_signed_16(0 to 3);
  signal x             : main_types.array_of_signed_16(0 to 3);
  signal x_0           : main_types.array_of_signed_16(0 to 3);
  signal rest          : main_types.array_of_signed_16(0 to 3);
  signal \c$vec\       : main_types.array_of_signed_16(0 to 4);
  signal \c$vec2\      : main_types.array_of_signed_16(0 to 3);
  signal \c$vec_0\     : main_types.array_of_signed_16(0 to 4);
  signal \c$vec2_0\    : main_types.array_of_signed_16(0 to 3);
  signal \c$vec_1\     : main_types.array_of_signed_16(0 to 4);
  signal \c$vec1\      : main_types.array_of_signed_16(0 to 3);
  signal \c$vec2_1\    : main_types.array_of_signed_16(0 to 3);

begin
  \c$x_app_arg\ <= main_types.array_of_signed_16'(0 to 4-1 =>  \c$arg\ );

  \c$vec\ <= main_types.array_of_signed_16'(signed'(to_signed(1,16)) & x);

  \c$vec2\ <= (\c$vec\(0 to \c$vec\'high - 1));

  -- zipWith begin
  zipwith : for i in x'range generate
  begin
    x(i) <= resize(\c$vec2\(i) * \c$x_app_arg\(i), 16);


  end generate;
  -- zipWith end

  \c$vec_0\ <= main_types.array_of_signed_16'(signed'(to_signed(1,16)) & x_0);

  \c$vec2_0\ <= (\c$vec_0\(0 to \c$vec_0\'high - 1));

  -- zipWith begin
  zipwith_0 : for i_0 in x_0'range generate
  begin
    x_0(i_0) <= resize(\c$vec2_0\(i_0) * \c$x_app_arg\(i_0), 16);


  end generate;
  -- zipWith end

  \c$vec_1\ <= main_types.array_of_signed_16'(signed'(to_signed(1,16)) & x);

  \c$vec1\ <= main_types.array_of_signed_16'( to_signed(3,16)
                                            , to_signed(4,16)
                                            , to_signed(5,16)
                                            , to_signed(2,16) );

  \c$vec2_1\ <= (\c$vec_1\(1 to \c$vec_1\'high));

  -- zipWith begin
  zipwith_1 : for i_1 in rest'range generate
  begin
    rest(i_1) <= resize(\c$vec1\(i_1) * \c$vec2_1\(i_1), 16);


  end generate;
  -- zipWith end

  result <= (to_signed(2,16) + rest(0)) + (rest(1) + (rest(2) + rest(3)));


end;

