-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.main_types.all;

entity topentity is
  port(-- clock
       clk    : in main_types.clk_system;
       -- reset
       rst    : in main_types.rst_system;
       x      : in signed(15 downto 0);
       result : out main_types.maybe);
end;

architecture structural of topentity is
  -- Polynomial.hs:146:1-9
  signal tup             : main_types.tup2;
  -- Polynomial.hs:146:1-9
  signal \c$tup_app_arg\ : main_types.array_of_signed_16(0 to 7);

begin
  result <= tup.tup2_sel1_maybe;

  f0mealy_tup : entity f0mealy
    port map
      ( result => tup
      , state  => \c$tup_app_arg\
      , x      => x );

  -- register begin
  topentity_register : block
    signal ctup_app_arg_reg : main_types.array_of_signed_16(0 to 7) := main_types.array_of_signed_16'( to_signed(2,16), to_signed(1,16), to_signed(0,16), to_signed(0,16), to_signed(0,16), to_signed(0,16), to_signed(0,16), to_signed(1,16) );
  begin
    \c$tup_app_arg\ <= ctup_app_arg_reg; 
    ctup_app_arg_r : process(clk,rst)
    begin
      if rst =  '1'  then
        ctup_app_arg_reg <= main_types.array_of_signed_16'( to_signed(2,16), to_signed(1,16), to_signed(0,16), to_signed(0,16), to_signed(0,16), to_signed(0,16), to_signed(0,16), to_signed(1,16) )
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      elsif rising_edge(clk) then
        ctup_app_arg_reg <= tup.tup2_sel0_array_of_signed_16
        -- pragma translate_off
        after 1 ps
        -- pragma translate_on
        ;
      end if;
    end process;
  end block;
  -- register end


end;

