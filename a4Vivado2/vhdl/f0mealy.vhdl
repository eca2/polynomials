-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.main_types.all;

entity f0mealy is
  port(state  : in main_types.array_of_signed_16(0 to 7);
       x      : in signed(15 downto 0);
       result : out main_types.tup2);
end;

architecture structural of f0mealy is
  signal \c$tupIn_app_arg\     : signed(15 downto 0);
  signal \c$tupIn_app_arg_0\   : signed(15 downto 0);
  signal \c$tupIn_app_arg_1\   : signed(15 downto 0);
  signal \c$tupIn_case_alt\    : main_types.tup2_0;
  signal \c$tupIn_app_arg_2\   : signed(15 downto 0);
  signal \c$tupIn_case_alt_0\  : main_types.tup2_0;
  signal \c$tupIn_case_alt_1\  : main_types.tup2_0;
  signal \c$case_alt\          : signed(15 downto 0);
  signal \c$tupIn_case_alt_2\  : main_types.tup2_0;
  -- Polynomial.hs:66:1-7
  signal \c$theALU_case_alt\   : signed(15 downto 0);
  signal \c$case_alt_0\        : signed(15 downto 0);
  signal \c$tupIn_case_alt_3\  : main_types.tup2_0;
  -- Polynomial.hs:66:1-7
  signal \c$theALU_case_alt_0\ : signed(15 downto 0);
  signal \c$case_alt_1\        : signed(15 downto 0);
  signal \c$tupIn_case_alt_4\  : main_types.tup2_0;
  -- Polynomial.hs:66:1-7
  signal \c$theALU_case_alt_1\ : signed(15 downto 0);
  -- Polynomial.hs:66:1-7
  signal \c$theALU_case_alt_2\ : signed(15 downto 0);
  signal \c$case_alt_2\        : signed(15 downto 0);
  -- Polynomial.hs:66:1-7
  signal \xPowReg\             : signed(15 downto 0);
  signal \c$tupIn\             : main_types.tup2_0;
  -- Polynomial.hs:66:1-7
  signal \c$theALU_case_alt_3\ : signed(15 downto 0);
  signal \c$case_alt_3\        : signed(15 downto 0);
  signal \c$case_alt_4\        : signed(15 downto 0);
  -- Polynomial.hs:66:1-7
  signal \accReg\              : signed(15 downto 0);
  -- Polynomial.hs:66:1-7
  signal \c$theALU_case_alt_4\ : signed(15 downto 0);
  signal \c$case_alt_5\        : signed(15 downto 0);
  signal \c$case_alt_6\        : signed(15 downto 0);
  signal \c$case_alt_7\        : signed(15 downto 0);
  -- Polynomial.hs:66:1-7
  signal state6                : boolean;
  -- Polynomial.hs:66:1-7
  signal state3                : boolean;
  signal result_0              : signed(15 downto 0);
  -- Polynomial.hs:66:1-7
  signal \c$theALU_case_alt_5\ : signed(15 downto 0);
  signal \c$case_alt_8\        : signed(15 downto 0);
  signal \c$case_alt_9\        : signed(15 downto 0);
  signal \c$case_alt_10\       : signed(15 downto 0);
  signal \c$case_alt_11\       : main_types.array_of_signed_16(0 to 4);
  -- Polynomial.hs:66:1-7
  signal state5                : boolean;
  -- Polynomial.hs:66:1-7
  signal state2                : boolean;
  -- Polynomial.hs:66:1-7
  signal \c$theALU_case_alt_6\ : signed(15 downto 0);
  signal \c$case_alt_12\       : signed(15 downto 0);
  signal \c$case_alt_13\       : signed(15 downto 0);
  signal \c$app_arg\           : signed(15 downto 0);
  signal \c$case_alt_14\       : main_types.array_of_signed_16(0 to 4);
  -- Polynomial.hs:66:1-7
  signal state4                : boolean;
  -- Polynomial.hs:66:1-7
  signal state1                : boolean;
  -- Polynomial.hs:66:1-7
  signal \xVecReg\             : main_types.array_of_signed_16(0 to 4);
  signal result_1              : signed(15 downto 0);
  signal \c$case_alt_15\       : signed(15 downto 0);
  signal \c$app_arg_0\         : signed(15 downto 0);
  signal \c$case_alt_16\       : main_types.array_of_signed_16(0 to 4);
  signal \c$<<+Out\            : main_types.array_of_signed_16(0 to 4);
  -- Polynomial.hs:66:1-7
  signal start                 : boolean;
  signal \c$app_arg_1\         : signed(15 downto 0);
  signal result_2              : main_types.array_of_signed_16(0 to 4);
  -- Polynomial.hs:66:1-7
  signal \stateReg\            : signed(15 downto 0);
  signal \c$app_arg_2\         : main_types.array_of_signed_16(0 to 4);
  -- Polynomial.hs:66:1-7
  signal done                  : boolean;
  signal \c$app_arg_3\         : main_types.maybe;
  signal \c$vec\               : main_types.array_of_signed_16(0 to 5);
  signal \c$<<+Out_projection\ : main_types.tup2_1;
  signal \c$vec_0\             : main_types.array_of_signed_16(0 to 4);

begin
  -- index begin
  indexvec : block
    signal vec_index : integer range 0 to 5-1;
  begin
    vec_index <= to_integer(to_signed(4,64))
    -- pragma translate_off
                 mod 5
    -- pragma translate_on
                 ;
    \c$tupIn_app_arg\ <= \xVecReg\(vec_index);
  end block;
  -- index end

  -- index begin
  indexvec_0 : block
    signal vec_index_0 : integer range 0 to 5-1;
  begin
    vec_index_0 <= to_integer(to_signed(3,64))
    -- pragma translate_off
                 mod 5
    -- pragma translate_on
                 ;
    \c$tupIn_app_arg_0\ <= \xVecReg\(vec_index_0);
  end block;
  -- index end

  -- index begin
  indexvec_1 : block
    signal vec_index_1 : integer range 0 to 5-1;
  begin
    vec_index_1 <= to_integer(to_signed(2,64))
    -- pragma translate_off
                 mod 5
    -- pragma translate_on
                 ;
    \c$tupIn_app_arg_1\ <= \xVecReg\(vec_index_1);
  end block;
  -- index end

  \c$tupIn_case_alt\ <= ( tup2_0_sel0_signed_0 => to_signed(5,16)
                        , tup2_0_sel1_signed_1 => \c$tupIn_app_arg_0\ ) when state6 else
                        ( tup2_0_sel0_signed_0 => to_signed(2,16)
                        , tup2_0_sel1_signed_1 => \c$tupIn_app_arg\ );

  -- index begin
  indexvec_2 : block
    signal vec_index_2 : integer range 0 to 5-1;
  begin
    vec_index_2 <= to_integer(to_signed(1,64))
    -- pragma translate_off
                 mod 5
    -- pragma translate_on
                 ;
    \c$tupIn_app_arg_2\ <= \xVecReg\(vec_index_2);
  end block;
  -- index end

  \c$tupIn_case_alt_0\ <= ( tup2_0_sel0_signed_0 => to_signed(4,16)
                          , tup2_0_sel1_signed_1 => \c$tupIn_app_arg_1\ ) when state5 else
                          \c$tupIn_case_alt\;

  \c$tupIn_case_alt_1\ <= ( tup2_0_sel0_signed_0 => to_signed(3,16)
                          , tup2_0_sel1_signed_1 => \c$tupIn_app_arg_2\ ) when state4 else
                          \c$tupIn_case_alt_0\;

  \c$case_alt\ <= to_signed(7,16) when state6 else
                  to_signed(0,16);

  \c$tupIn_case_alt_2\ <= ( tup2_0_sel0_signed_0 => \xPowReg\
                          , tup2_0_sel1_signed_1 => x ) when state3 else
                          \c$tupIn_case_alt_1\;

  \c$theALU_case_alt\ <= \c$theALU_case_alt_2\ when done else
                         to_signed(0,16);

  \c$case_alt_0\ <= to_signed(6,16) when state5 else
                    \c$case_alt\;

  \c$tupIn_case_alt_3\ <= ( tup2_0_sel0_signed_0 => \xPowReg\
                          , tup2_0_sel1_signed_1 => x ) when state2 else
                          \c$tupIn_case_alt_2\;

  \c$theALU_case_alt_0\ <= \c$theALU_case_alt_2\ when state6 else
                           \c$theALU_case_alt\;

  \c$case_alt_1\ <= to_signed(5,16) when state4 else
                    \c$case_alt_0\;

  \c$tupIn_case_alt_4\ <= ( tup2_0_sel0_signed_0 => \xPowReg\
                          , tup2_0_sel1_signed_1 => x ) when state1 else
                          \c$tupIn_case_alt_3\;

  \c$theALU_case_alt_1\ <= \c$theALU_case_alt_2\ when state5 else
                           \c$theALU_case_alt_0\;

  \c$theALU_case_alt_2\ <= \accReg\ + result_0;

  \c$case_alt_2\ <= to_signed(4,16) when state3 else
                    \c$case_alt_1\;

  -- index begin
  indexvec_3 : block
    signal vec_index_3 : integer range 0 to 8-1;
  begin
    vec_index_3 <= to_integer(to_signed(1,64))
    -- pragma translate_off
                 mod 8
    -- pragma translate_on
                 ;
    \xPowReg\ <= state(vec_index_3);
  end block;
  -- index end

  \c$tupIn\ <= ( tup2_0_sel0_signed_0 => \xPowReg\
               , tup2_0_sel1_signed_1 => x ) when start else
               \c$tupIn_case_alt_4\;

  \c$theALU_case_alt_3\ <= \c$theALU_case_alt_2\ when state4 else
                           \c$theALU_case_alt_1\;

  \c$case_alt_3\ <= result_1 when state3 else
                    \xPowReg\;

  \c$case_alt_4\ <= to_signed(3,16) when state2 else
                    \c$case_alt_2\;

  -- index begin
  indexvec_4 : block
    signal vec_index_4 : integer range 0 to 8-1;
  begin
    vec_index_4 <= to_integer(to_signed(0,64))
    -- pragma translate_off
                 mod 8
    -- pragma translate_on
                 ;
    \accReg\ <= state(vec_index_4);
  end block;
  -- index end

  \c$theALU_case_alt_4\ <= result_0 when state3 else
                           \c$theALU_case_alt_3\;

  \c$case_alt_5\ <= result_1 when done else
                    \accReg\;

  \c$case_alt_6\ <= result_1 when state2 else
                    \c$case_alt_3\;

  \c$case_alt_7\ <= to_signed(2,16) when state1 else
                    \c$case_alt_4\;

  state6 <= \stateReg\ = to_signed(6,16);

  state3 <= \stateReg\ = to_signed(3,16);

  result_0 <= resize(\c$tupIn\.tup2_0_sel0_signed_0 * \c$tupIn\.tup2_0_sel1_signed_1, 16);

  \c$theALU_case_alt_5\ <= result_0 when state2 else
                           \c$theALU_case_alt_4\;

  \c$case_alt_8\ <= result_1 when state6 else
                    \c$case_alt_5\;

  \c$case_alt_9\ <= result_1 when state1 else
                    \c$case_alt_6\;

  \c$case_alt_10\ <= to_signed(1,16) when start else
                     \c$case_alt_7\;

  \c$case_alt_11\ <= \c$<<+Out\ when state3 else
                     \xVecReg\;

  state5 <= \stateReg\ = to_signed(5,16);

  state2 <= \stateReg\ = to_signed(2,16);

  \c$theALU_case_alt_6\ <= result_0 when state1 else
                           \c$theALU_case_alt_5\;

  \c$case_alt_12\ <= result_1 when state5 else
                     \c$case_alt_8\;

  \c$case_alt_13\ <= result_1 when start else
                     \c$case_alt_9\;

  \c$app_arg\ <= to_signed(0,16) when done else
                 \c$case_alt_10\;

  \c$case_alt_14\ <= \c$<<+Out\ when state2 else
                     \c$case_alt_11\;

  state4 <= \stateReg\ = to_signed(4,16);

  state1 <= \stateReg\ = to_signed(1,16);

  -- select begin
  select_r : for i in \xVecReg\'range generate
    \xVecReg\(i) <= state(3+(1*i));
  end generate;
  -- select end

  result_1 <= result_0 when start else
              \c$theALU_case_alt_6\;

  \c$case_alt_15\ <= result_1 when state4 else
                     \c$case_alt_12\;

  \c$app_arg_0\ <= to_signed(1,16) when done else
                   \c$case_alt_13\;

  \c$case_alt_16\ <= \c$<<+Out\ when state1 else
                     \c$case_alt_14\;

  \c$vec\ <= (main_types.array_of_signed_16'(main_types.array_of_signed_16'(\xVecReg\) & main_types.array_of_signed_16'(main_types.array_of_signed_16'(0 => result_1))));

  \c$<<+Out_projection\ <= (\c$vec\(0 to 1-1),\c$vec\(1 to \c$vec\'high));

  \c$<<+Out\ <= \c$<<+Out_projection\.tup2_1_sel1_array_of_signed_16_1;

  start <= \stateReg\ = to_signed(0,16);

  \c$vec_0\ <= main_types.array_of_signed_16'( to_signed(2,16)
                                             , to_signed(3,16)
                                             , to_signed(4,16)
                                             , to_signed(5,16)
                                             , to_signed(2,16) );

  \c$app_arg_1\ <=  \c$vec_0\(0)  when start else
                   \c$case_alt_15\;

  result_2 <= \c$<<+Out\ when start else
              \c$case_alt_16\;

  -- index begin
  indexvec_5 : block
    signal vec_index_5 : integer range 0 to 8-1;
  begin
    vec_index_5 <= to_integer(to_signed(2,64))
    -- pragma translate_off
                 mod 8
    -- pragma translate_on
                 ;
    \stateReg\ <= state(vec_index_5);
  end block;
  -- index end

  \c$app_arg_2\ <= main_types.array_of_signed_16'( to_signed(0,16)
                                                 , to_signed(0,16)
                                                 , to_signed(0,16)
                                                 , to_signed(0,16)
                                                 , to_signed(1,16) ) when done else
                   result_2;

  done <= \stateReg\ = to_signed(7,16);

  \c$app_arg_3\ <= std_logic_vector'("1" & (std_logic_vector(result_1))) when done else
                   std_logic_vector'("0" & "----------------");

  result <= ( tup2_sel0_array_of_signed_16 => main_types.array_of_signed_16'(main_types.array_of_signed_16'(main_types.array_of_signed_16'( \c$app_arg_1\, \c$app_arg_0\, \c$app_arg\ )) & main_types.array_of_signed_16'(\c$app_arg_2\))
            , tup2_sel1_maybe => \c$app_arg_3\ );


end;

