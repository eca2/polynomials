library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package main_types is

  type tup3 is record
    tup3_sel0_signed_0 : signed(15 downto 0);
    tup3_sel1_signed_1 : signed(15 downto 0);
    tup3_sel2_signed_2 : signed(15 downto 0);
  end record;
  function toSLV (s : in signed) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return signed;
  function toSLV (p : main_types.tup3) return std_logic_vector;
  function fromSLV (slv : in std_logic_vector) return main_types.tup3;
end;

package body main_types is
  function toSLV (s : in signed) return std_logic_vector is
  begin
    return std_logic_vector(s);
  end;
  function fromSLV (slv : in std_logic_vector) return signed is
  begin
    return signed(slv);
  end;
  function toSLV (p : main_types.tup3) return std_logic_vector is
  begin
    return (toSLV(p.tup3_sel0_signed_0) & toSLV(p.tup3_sel1_signed_1) & toSLV(p.tup3_sel2_signed_2));
  end;
  function fromSLV (slv : in std_logic_vector) return main_types.tup3 is
  alias islv : std_logic_vector(0 to slv'length - 1) is slv;
  begin
    return (fromSLV(islv(0 to 15)),fromSLV(islv(16 to 31)),fromSLV(islv(32 to 47)));
  end;
end;

