-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.main_types.all;

entity f0p is
  port(x      : in signed(15 downto 0);
       result : out signed(15 downto 0));
end;

architecture structural of f0p is


begin
  result <= ((((resize(to_signed(2,16) * (resize((to_signed(to_integer(x) ** 4, 64)),16)), 16)) + (resize(to_signed(5,16) * (resize((to_signed(to_integer(x) ** 3, 48)),16)), 16))) + (resize(to_signed(4,16) * (resize((to_signed(to_integer(x) ** 2, 32)),16)), 16))) + (resize(to_signed(3,16) * (resize((to_signed(to_integer(x) ** 1, 16)),16)), 16))) + to_signed(2,16);


end;

