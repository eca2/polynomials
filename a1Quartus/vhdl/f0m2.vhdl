-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.main_types.all;

entity f0m2 is
  port(x      : in signed(15 downto 0);
       result : out signed(15 downto 0));
end;

architecture structural of f0m2 is
  signal \c$app_arg\   : signed(15 downto 0);
  signal \c$app_arg_0\ : signed(15 downto 0);

begin
  \c$app_arg\ <= resize(x * x, 16);

  \c$app_arg_0\ <= resize(\c$app_arg\ * x, 16);

  result <= ((((resize(to_signed(2,16) * (resize(\c$app_arg_0\ * x, 16)), 16)) + (resize(to_signed(5,16) * \c$app_arg_0\, 16))) + (resize(to_signed(4,16) * \c$app_arg\, 16))) + (resize(to_signed(3,16) * x, 16))) + to_signed(2,16);


end;

