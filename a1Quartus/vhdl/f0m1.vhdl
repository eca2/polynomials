-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.main_types.all;

entity f0m1 is
  port(x      : in signed(15 downto 0);
       result : out signed(15 downto 0));
end;

architecture structural of f0m1 is


begin
  result <= ((((resize((resize((resize((resize(to_signed(2,16) * x, 16)) * x, 16)) * x, 16)) * x, 16)) + (resize((resize((resize(to_signed(5,16) * x, 16)) * x, 16)) * x, 16))) + (resize((resize(to_signed(4,16) * x, 16)) * x, 16))) + (resize(to_signed(3,16) * x, 16))) + to_signed(2,16);


end;

