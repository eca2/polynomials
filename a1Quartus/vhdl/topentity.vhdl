-- Automatically generated VHDL-93
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.all;
use work.main_types.all;

entity topentity is
  port(x        : in signed(15 downto 0);
       result_0 : out signed(15 downto 0);
       result_1 : out signed(15 downto 0);
       result_2 : out signed(15 downto 0));
end;

architecture structural of topentity is
  signal \c$app_arg\   : signed(15 downto 0);
  signal \c$app_arg_0\ : signed(15 downto 0);
  signal \c$app_arg_1\ : signed(15 downto 0);
  signal result        : main_types.tup3;

begin
  f0m2_capp_arg : entity f0m2
    port map
      (result => \c$app_arg\, x      => x);

  f0m1_capp_arg_0 : entity f0m1
    port map
      (result => \c$app_arg_0\, x      => x);

  f0p_capp_arg_1 : entity f0p
    port map
      (result => \c$app_arg_1\, x      => x);

  result <= ( tup3_sel0_signed_0 => \c$app_arg_1\
            , tup3_sel1_signed_1 => \c$app_arg_0\
            , tup3_sel2_signed_2 => \c$app_arg\ );

  result_0 <= result.tup3_sel0_signed_0;

  result_1 <= result.tup3_sel1_signed_1;

  result_2 <= result.tup3_sel2_signed_2;


end;

